import math as ma
import statistics as stat
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import scipy as sp
import scipy.optimize as optim


def get_counts(file):
    if file[-len('.Spe'):] != '.Spe':
        file += '.Spe'
    if file[:len('data')] != 'data':
        file = os.path.join('data', file)

    with open(file) as temp:
        data = temp.read().split('\n')
    data = data[12:524]
    data = [int(count) for count in data]
    return data


def remove_background(background_file):
    background = get_counts(background_file)
    background = [count/9. for count in background]
    output = [count_raw-count_bg for count_raw, count_bg
                                    in zip(data, background)]
    return output


def get_peak(counts, start=20, end=512):
    channels = list(range(512))
    #This take the center channel such that it sum with its two neighbors is
    #biggest.
    channel_peak = -10
    max_total_count = -10
    recent_counts = [0, 0]
    for channel, count in zip(channels, counts):
        if (channel >= start and channel < end):
            total_count = count + recent_counts[0] + recent_counts[1]
            if (total_count > max_total_count):
                max_total_count = total_count
                channel_peak = channel - 1
        recent_counts[1] = recent_counts[0]
        recent_counts[0] = count
    return channel_peak


def comp_peak_counts(data, peak=-1):
    peak = (get_peak(file) if peak == -1 else peak)
    assert peak > 4

    sum = 0
    for channel in range(peak-4, peak+5):
        sum += data[channel]
    return sum


def chi_squared_test(data):
    mean = stat.mean(data)
    chi_squared = 0
    for sample in data:
        chi_squared += (sample - mean)**2
    chi_squared /= mean
    return len(data), chi_squared
