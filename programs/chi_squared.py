import lib


data = [30054, 30112, 30139, 30186, 30231]

if __name__ ==  '__main__':
    num_samples, χ2 = lib.chi_squared_test(data)
    print(f'Over {num_samples} samples, the χ2 is {round(χ2, 3)}')
