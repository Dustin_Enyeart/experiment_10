import math as ma
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as optim
import statistics as stat


def comp_rates(data: '[distance, weight, time, count]'):
    rates = []
    for _, weight, time, count in data:
        real_time = time_period - dead_time*count
        rate = count/real_time #Adjust for dead time.
        rate -= background/real_time #Adjust for background.
        half_life = 54.2*60.
        rate = rate*np.exp(time*np.log(2.)/half_life) #Adjust for time out of
                                                      #assembly.
        rate /= weight #Adjust for weight.
        rates.append(rate)
    return rates


def get_distances(data: '[distance, weight, time, count]'):
    distances = []
    for distance, _, _, _ in data:
        distances.append(distance)
    return distances


#def flux(x, y, z, C, α1, α2, γ):
#    return C*np.sin(α1*x)*np.sin(α2*y)*np.exp(-γ*z)


def find_α(distances, rates):
    def to_find_α(x, C, α):
        return C*np.sin(α*x)

    C_start = max(rates) #Beforehand estimated peak.
    α_start = np.pi/68.  #Beforehand estimated half of the period.

    params = optim.curve_fit(to_find_α, distances, rates,
                             p0 = [C_start, α_start])

    C = params[0][0]
    α = params[0][1]

    plt.scatter(distances, rates, marker='o', label='data', color='red')
    computed_rates = [to_find_α(x, C, α) for x in distances]
    plt.plot(distances, computed_rates, label='fit')
    plt.xlabel('distance (in)')
    plt.ylabel('rate per gram (cps)')
    plt.legend()
    plt.show()

    return α


def find_γ(distances, rates):
    def to_find_γ(z, C, γ):
        return C*np.exp(-γ*z)
    def to_find_log_γ(z, C , γ):
        return C - γ*z

    log_rates = [np.log(rate) for rate in rates]

    params = optim.curve_fit(to_find_log_γ, distances, log_rates)

    C = params[0][0]
    γ = params[0][1]

    plt.scatter(distances, rates, marker='o', label='data', color='red')
    computed_rates = [to_find_γ(x, C, γ) for x in distances]
    plt.plot(distances, computed_rates, label='fit')
    plt.xlabel('distance (in)')
    plt.ylabel('rate per gram (cps)')
    plt.legend()
    plt.show()

    return γ


###############################################################################
#Data
###############################################################################

#These are only for my measurements, that is, those along the x coordinate.
time_period = 60. #In seconds.
dead_time = 800*10**(-6)
background = 6

#The y and z coordinates for each of these are 34. and 44. respectively.
data_x = [#distance (in) | weight (g) | time (s)  | counts
         [2.25 + 0*4,       .624,       7*60 + 26,   116], #1
         [2.25 + 1*4.,      .566,       5*60 + 8,    261], #2
         [2.25 + 2*4.,      .623,       11*60 + 8,   319], #3
         [2.25 + 3*4.,      .555,       13*60 + 39,  407], #4
         [2.25 + 4*4.,      .549,       15*60 + 55,  526], #5
         [2.25 + 5*4.,      .630,       18*60 + 41,  628], #6
         [2.25 + 6*4.,      .629,       20*60 + 35,  576], #7
         [2.25 + 7*4.,      .575,       22*60 + 57,  615], #8
         [2.25 + 8*4.,      .575,       24*60 + 55,  568], #9
         [2.25 + 9*4.,      .585,       26*60 + 39,  648], #10
         [2.25 + 10*4.,     .533,       29*60 + 0,   551], #11
         [2.25 + 11*4.,     .585,       30*60 + 38,  495], #12
         [2.25 + 12*4.,     .572,       32*60 + 21,  424], #13
         [2.25 + 13*4.,     .625,       34*60 + 8,   346], #14
         [2.25 + 14*4.,     .559,       35*60 + 54,  248], #15
         [2.25 + 15*4.,     .574,       37*60 + 50,  178], #16
         [2.25 + 16*4.,     .560,       39*60 + 39,  84],  #17
         ]

distances_x = get_distances(data_x)
rates_x = comp_rates(data_x)

#Covert to centimeters.
#for i in range(len(data_x)):
#    data_x[i][0] = data_x[i][0]

distances_y = [-80, -70, -60, -50, -40, -30, -20, -10, 0, 10, 20, 30, 40, 50,
                60, 70, 80]
distances_y = [(distance+80.)/2.54 for distance in distances_y]
rates_y = [4.27, 11.52, 18.11, 24.57, 31.50, 36.02, 38.45, 44.22, 44.32,
            44.61, 39.43, 35.44, 29.18, 24.46, 17.66, 11.59, 4.72]

distances_z = [5.08, 15.24, 25.4, 35.56, 45.72, 55.88, 66.04, 76.2, 86.36,
               96.52, 106.68, 116.84, 127, 137.16, 147.32, 157.48, 167.64,
               177.8, 187.96]
distances_z = [distance/2.54 for distance in distances_z]
rates_z = [0.33349962, 0.9619893, 1.20128602, 1.76365221, 2.05334996,
           2.71115155, 3.20766746, 4.53702608, 4.76626828, 6.58588164,
           7.61238224, 8.9888288, 11.2414434, 13.7836061, 16.3285719, 20.1248561, 23.8310654, 29.6781896, 33.97077]


###############################################################################

if __name__ == '__main__':
    α_1 = find_α(distances_x, rates_x)
    print(f'The value of α_1 is {round(α_1, 4)}.')
    α_2 = find_α(distances_y, rates_y)
    print(f'The value of α_2 is {round(α_2, 4)}.')
    γ = find_γ(distances_z, rates_z)
    print(f'The value of γ is {round(γ, 4)}.')
    material_buckling = (α_1**2 + α_2**2 - γ**2)**.5
    print(f'The material buckling is {material_buckling}.')
    req_len = (3**.5 * np.pi)/material_buckling
    print('For the reactor to go critical, the lengths of its sides'
          + f'must be {req_len}.')
